<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2016-2020 Harald Sitter <sitter@kde.org>
-->

# Building

- set GOPATH in envrionment
- install go
- make run

# Installation

- adjust systemd/* as you need it
- copy systemd/* to ~/.config/systemd/user/ or a system location depending on
  whether you want to run it as system service or user service
- when using a user service make sure to enable login lingering for that
  user with `loginctl enable-linger $username`
- enable the systemd service `systemctl --user enable neon-contents-grapple.service`
  (or without --user for system service)
- start it `systemctl --user start neon-contents-grapple.service`
- make sure it is running `systemctl --user status neon-contents-grapple.service`
- it should now persist through logouts and due to linking into default target is should autostart on reboots

## Documentation

Documentation is generated from in-code markup using http://apidocjs.com/

`npm install apidoc` will install the thing into node_modules/, `rake doc` will make it

## Config

The daemon requires a config to run in `~/.config/neon-contents-grapple.yaml` something LIKE

```
user: me
password: me
database: neon-contents2
sslmode: disable
```

## notes

- binary should be run through systemd

# Deploy

to deploy updates to KDE (you need access to neonapis user)

- `bundle install`
- `make deploy`

the deploy.rb helper will rsync the built binary and restart the systemd service.
