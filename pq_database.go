// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>

package main

import (
	"database/sql"

	_ "github.com/lib/pq"
)

type PQDatabase struct {
	pq *sql.DB
}

func NewPQDatabase() *PQDatabase {
	db, err := sql.Open("postgres", config.DBOpenString())
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS archives (
	contents_id text NOT NULL UNIQUE,
	last_date text,
	data_table text);`)
	if err != nil {
		panic(err)
	}

	return &PQDatabase{
		pq: db,
	}
}
