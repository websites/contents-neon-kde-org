#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2016-2020 Harald Sitter <sitter@kde.org>

require 'net/ssh'

bin = 'neon-contents-grapple'
user = 'neonapis'
host = 'contents.neon.kde.org'
home = "/home/#{user}"
path = "#{home}/bin/"
systemd = "#{home}/.config/systemd/user"

system('go', 'build', '-v', '-o', bin) || raise

system('rsync', '-avz', '--progress',
       '-e', 'ssh', bin, "#{user}@#{host}:#{path}") || raise

system('ssh', "#{user}@#{host}",
       "mkdir -p #{home}/.config/systemd/user") || raise
system('rsync', '-avz', '--progress',
       '-e', 'ssh', 'systemd/neon-contents-grapple.service',
       "#{user}@#{host}:#{systemd}") || raise
system('ssh', "#{user}@#{host}",
       'systemctl --user daemon-reload') || raise
system('ssh', "#{user}@#{host}",
       'systemctl --user restart neon-contents-grapple.service') || raise

system('rsync', '-avz', '--progress',
       '-e', 'ssh', 'contents-doc', "#{user}@#{host}:#{home}/data/")
