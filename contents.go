// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2016-2020 Harald Sitter <sitter@kde.org>

package main

import (
	"bufio"
	"compress/gzip"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/lib/pq"

	"golang.org/x/net/html/charset"
)

type Contents struct {
	uri        string
	id         string
	table      string
	insertStmt *sql.Stmt
}

func NewContents(uri string) *Contents {
	contents := &Contents{
		uri: uri,
	}
	contents.id = contents.getID()
	contents.table = strings.Replace(strings.Replace(contents.id, "/", "_", -1),
		".", "_", -1)
	return contents
}

func (contents *Contents) Get() error {
	var lastDate string

	var s sql.NullString
	pqDB.QueryRow("SELECT last_date FROM archives WHERE contents_id = $1;", contents.id).Scan(&s)
	if s.Valid {
		lastDate = s.String
	}

	fmt.Println("Downloading " + contents.uri)
	client := &http.Client{}
	req, err := http.NewRequest("GET", contents.uri, nil)
	if lastDate != "" {
		req.Header.Set("If-Modified-Since", string(lastDate))
	}
	if err != nil {
		panic(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.ContentLength == 0 {
		fmt.Println("Failed to download contents (may not have been modified)! " + resp.Status)
		return nil
	}

	// Writer the body to file
	// Reading from Body.Resp via Gzip and Bufio is substantially slower
	// than first downloading the entire body and reading from local. I am
	// not entirely sure why that is since bufio should make it fast :(
	tmpfile, err := ioutil.TempFile("", "neon-contents-grapple")
	if err != nil {
		return err
	}
	defer os.Remove(tmpfile.Name()) // clean up
	defer tmpfile.Close()
	_, err = io.Copy(tmpfile, resp.Body)
	if err != nil {
		return err
	}
	tmpfile.Seek(0, 0)

	gzip, err := gzip.NewReader(bufio.NewReader(tmpfile))
	if err != nil {
		panic(err)
	}
	defer gzip.Close()

	// Ubuntu's Contents is ISO for unknown reasons, use the builtin HTML charset
	// converter to handle this without excessive amounts of code.
	charsetReader, err := charset.NewReader(gzip, "text/plain")
	if err != nil {
		panic(err)
	}

	contents.process(bufio.NewReaderSize(charsetReader, 64*1024*1024), contents.id)

	_, err = pqDB.Exec(`
	INSERT INTO archives (contents_id, last_date, data_table) VALUES ($1, $2, $3)
		ON CONFLICT ON CONSTRAINT archives_contents_id_key
			DO UPDATE SET last_date=$2, data_table=$3;
		`, contents.id, resp.Header.Get("Date"), contents.table)
	if err != nil {
		panic(err)
	}

	return nil
}

func (contents *Contents) parseLine(line string) (string, string) {
	// FIXME: this is fairly bad. What we should be doing is find the last SPACE
	// (not just unicode.isspace which could be tab or whatever) and split at it
	// the possibly clean up the file by dropping trailing spaces.
	// I am thinking a file name could possibly have a \t inside, so splitting
	// this liberally can have adverse effects.
	parts := strings.Fields(line)
	if !(len(parts) >= 2) {
		panic("invalid line: " + line)
	}
	location, parts := parts[len(parts)-1], parts[:len(parts)-1] // pop
	file := strings.Join(parts, " ")                             // join to retain spaces in path

	// Ditch / if there even is one
	parts = strings.Split(location, "/")
	location = parts[len(parts)-1]

	file = strings.TrimSpace(file)
	location = strings.TrimSpace(location)

	return file, location
}

func (contents *Contents) processLine(line string) {
	file, location := contents.parseLine(line)
	_, err := contents.insertStmt.Exec(location, file)
	if err != nil {
		panic(err.Error() + " '" + file + "' " + location)
	}
}

func (contents *Contents) isRow(line string) bool {
	// A row is divided into 2 columns separated by one or more spaces.
	// The first part is the FILE and the second part the LOCATION. FILE may
	// contain just about anything, LOCATION is essentially a variation of pkgname
	// and fairly restricted. So, any line with spaces will do so long as the last
	// "word" is only comprised of "safe" characters.
	parts := strings.Split(line, " ")
	if len(parts) < 2 {
		return false // Not at least one space
	}
	location := parts[len(parts)-1]
	return contents.isLocationColumn(location)
}

func (contents *Contents) isLocationColumn(line string) bool {
	if strings.Contains(line, " ") {
		return false // pkgnames have no spaces. bail early
	}

	// Split the line into potential package parts. A Location may be
	// one or more package names seperated by commas. Each package name may be
	// prefixed with the package section separated by a slash.
	// NB: split is always at least one element long (no comma found).
	parts := strings.Split(line, ",")

	// locationRegexp := regexp.MustCompile(`\w+`)                     // pkg
	// `section/pkg` or `pkg`
	locationWithSectionRegexp := regexp.MustCompile(`(\w+/\w+|\w+)`)
	for _, part := range parts {
		matched := locationWithSectionRegexp.MatchString(part)
		if !matched {
			return false
		}
	}

	return true
}

func spoolOffLines(count int, reader *bufio.Reader) {
	for i := 0; i < count; i++ {
		_, err := reader.ReadSlice('\n')
		if err == io.EOF {
			panic("Spooled to unexpected end of file! Contents malformed?")
		} else if err != nil {
			panic(err)
		}
	}
}

func (contents *Contents) findStart(reader *bufio.Reader) bool {
	// https://wiki.debian.org/DebianRepository/Format#A.22Contents.22_indices
	// Contents indices begin with zero or more lines of free form text followed
	// by a table mapping filenames to one or more packages.
	// The table SHALL have two columns, separated by one or more spaces.
	// The first row of the table SHOULD have the columns "FILE" and "LOCATION"

	// This makes parsing super awkward. Thanks Debian!
	// "This could could be a file location and packagename"
	//   => ["This could could be a file location and", "packagename"]
	// "Or it could be free form text"
	//   => ["Or it could be free form", "text"]

	// We peek ahead a whole bunch of bytes to get a good sample size. This
	// is done because resetting readers is a major hassle becuase we have
	// multiple readers stacked and they'd all need a reset. Peeking ahead
	// seems just as good. This is limited by the buffer size of the bufio, so
	// this implicitly requiers the reader to be constructed with a suitably
	// large size (otherwise we'll panic here).
	// Once we've found the actual start line we'll spool the reader to the
	// line after that which will put it at the beginning of the actual data
	// rows.

	// Arbitrary count. A peek sample smaller than this is considered garbage.
	// It's also the maximum amount of lines we'll look at.
	wantedLineCount := 128

	// Arbitrary, should be large enough to fit 128 lines of fairly huge byte
	// size.
	peekSize := 1024 * 512
	bytes, err := reader.Peek(peekSize)
	// May err EOF if less than the requested size was read.
	if err != nil && err != io.EOF {
		panic(err)
	}
	lines := strings.Split(string(bytes[:]), "\n")
	if len(lines) < wantedLineCount+1 {
		// This is wanted+1 because we need a complete/valid line lest it fails the
		// row check becuase it is incomplete!
		panic("Not enough sample lines in our peek! " + strconv.Itoa(len(lines)))
	}

	// Let's try to find a header line first. If we found one whatever follows is
	// the data columns.
	// var line string
	lastInvalidLineIndex := 0
	for index, line := range lines {
		if index > wantedLineCount-1 {
			break
		}
		foundStart := strings.HasPrefix(line, "FILE") && strings.Contains(line, "LOCATION")
		if foundStart {
			spoolOffLines(index+1, reader)
			return true
		}

		// Is this line a valid row?
		// Skip over empty lines
		if strings.TrimSpace(line) != "" && !contents.isRow(line) {
			fmt.Println("line not valid " + strconv.Itoa(index))
			lastInvalidLineIndex = index
		}
	}

	if lastInvalidLineIndex == wantedLineCount-1 {
		panic("Couldnt find contents lines. Malformed file?")
	}

	fmt.Println("Found no header. Seeking to first valid line " + strconv.Itoa(lastInvalidLineIndex))

	// If we found no header we'll rest the bufio and seek beyond the last invalid
	// line. The following line (should) be the first valid line.

	spoolOffLines(lastInvalidLineIndex, reader)

	return true
}

func (contents *Contents) process(reader *bufio.Reader, hash string) {
	fmt.Println(hash)

	table := contents.table
	fmt.Println(table)
	_, err := pqDB.Exec("CREATE TABLE IF NOT EXISTS " + table + ` (
	package text NOT NULL,
	path text NOT NULL
);
		`)
	if err != nil {
		panic(err)
	}

	// Cannot use LIKE here as the key references would need bending.
	_, err = pqDB.Exec("DROP TABLE IF EXISTS " + table + "_tmp;" +
		"CREATE UNLOGGED TABLE IF NOT EXISTS " + table + `_tmp (
	package text NOT NULL,
	path text NOT NULL
);
		`)
	if err != nil {
		panic(err)
	}

	txn, err := pqDB.Begin()
	if err != nil {
		panic(err)
	}

	// stmt, err := pqDB.Prepare(`
	// WITH source AS ( INSERT INTO sources_tmp (name) VALUES ($1) ON CONFLICT ON CONSTRAINT sources_tmp_name_key DO UPDATE SET name=$1 RETURNING * )
	// INSERT INTO files_tmp (path, source_id) SELECT $2, source.id FROM source;
	// 	`)
	stmt, err := txn.Prepare(pq.CopyIn(table+"_tmp", "package", "path"))
	if err != nil {
		panic(err)
	}
	contents.insertStmt = stmt

	if !contents.findStart(reader) {
		panic("Failed to find file start. Malformed file?")
	}

	input := make(chan string)

	go func() {
		var line string
		var err error
		for {
			line, err = reader.ReadString('\n')
			if err == nil {
				input <- line
				continue
			} else if err == io.EOF {
				break
			}
			panic(err)
		}
		close(input)
	}()

	var processorWg sync.WaitGroup
	// FIXME: lowered to not hit connection cap, may be not necessary with transactions
	for i := 0; i < 2048; i++ {
		processorWg.Add(1)
		go contents.readLine(input, &processorWg, i)
	}

	processorWg.Wait()

	if err = stmt.Close(); err != nil {
		panic(err)
	}
	if err = txn.Commit(); err != nil {
		panic(err)
	}

	_, err = pqDB.Exec(fmt.Sprintf(`
BEGIN;
DROP TABLE %[1]s;
ALTER TABLE %[1]s_tmp RENAME to %[1]s;
COMMIT;`, table))
	if err != nil {
		panic(err)
	}
}

func (contents *Contents) readLine(input chan string, wg *sync.WaitGroup, i int) {
	defer wg.Done()
	for line := range input {
		// fmt.Println("worker " + strconv.Itoa(i) + " " + line)
		contents.processLine(line)
	}
}

func (contents *Contents) getID() string {
	u, err := url.Parse(contents.uri)
	if err != nil {
		panic(err)
	}
	uHost := u.Host
	uPath := filepath.Dir(u.Path) // ../ of file
	if strings.Contains(uPath, "main") {
		uPath = filepath.Dir(uPath) // ../../ of file for aptly hack
	}
	return filepath.Join(uHost, uPath)
}
