// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

package main

import (
	"bufio"
	"compress/gzip"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/net/html/charset"
)

func init() {
}

type contentsTestCase struct {
}

func TestStartFoundNoHeader(t *testing.T) {
	c := Contents{}

	path := filepath.Join("testdata", "Contents-no-header")
	reader, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	defer reader.Close()

	bufReader := bufio.NewReaderSize(reader, 64*1024*1024)

	assert.Equal(t, true, c.findStart(bufReader))

	line, err := bufReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "bin/afio                                                    multiverse/utils/afio\n", line)
}

func TestStartFoundHeader(t *testing.T) {
	c := Contents{}

	path := filepath.Join("testdata", "Contents-header")
	reader, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	defer reader.Close()

	bufReader := bufio.NewReaderSize(reader, 64*1024*1024)

	assert.Equal(t, true, c.findStart(bufReader))

	line, err := bufReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "bin/afio                                                    multiverse/utils/afio\n", line)
}

func TestStartFoundHeaderAndFreeForm(t *testing.T) {
	c := Contents{}

	path := filepath.Join("testdata", "Contents")
	reader, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	defer reader.Close()

	bufReader := bufio.NewReaderSize(reader, 64*1024*1024)

	assert.Equal(t, true, c.findStart(bufReader))

	line, err := bufReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "bin/afio                                                    multiverse/utils/afio\n", line)
}

// NB: this is copy paste from Get() which also streams through gzip and charset
func TestStartFoundGZipWithHeaderAndFreeForm(t *testing.T) {
	c := Contents{}

	path := filepath.Join("testdata", "Contents.gz")
	reader, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	defer reader.Close()

	gzip, err := gzip.NewReader(bufio.NewReader(reader))
	if err != nil {
		t.Fatal(err)
	}
	defer gzip.Close()

	// Ubuntu's Contents is ISO for unknown reasons, use the builtin HTML charset
	// converter to handle this without excessive amounts of code.
	charsetReader, err := charset.NewReader(gzip, "text/plain")
	if err != nil {
		t.Fatal(err)
	}

	bufReader := bufio.NewReaderSize(charsetReader, 64*1024*1024)

	lineNumber := c.findStart(bufReader)
	assert.Equal(t, true, lineNumber)

	line, err := bufReader.ReadString('\n')
	assert.NoError(t, err)
	assert.Equal(t, "bin/afio                                                    multiverse/utils/afio\n", line)
}
