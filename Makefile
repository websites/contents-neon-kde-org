# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>

clean:
	rm -rf contents-doc

node_modules/.bin/apidoc:
	npm install apidoc

deploy: test contents-doc
	ruby deploy.rb

contents-doc: node_modules/.bin/apidoc
	node_modules/.bin/apidoc \
		--debug \
		-e node_modules \
		-e vendor \
		-e contents-doc \
		-o contents-doc \
		-i .

neon-contents-grapple:
	go build -o neon-contents-grapple -v

run: neon-contents-grapple
	./neon-contents-grapple

test:
	go test -v ./...

.PHONY: deploy run tets neon-contents-grapple contents-doc # doc and the bin are phony because they need to be updated!
