module invent.kde.org/websites/contents-neon-kde-org.git

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/lib/pq v1.8.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	gopkg.in/yaml.v2 v2.3.0
)
