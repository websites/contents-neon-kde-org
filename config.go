// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>

package main

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"

	"os"
	"path/filepath"
)

// Configuration container
type Configuration struct {
	DBUser     string `yaml:"user"`
	DBPassword string `yaml:"password"`
	DBName     string `yaml:"database"`
	SSLMode    string `yaml:"sslmode"`
}

var config = LoadConfig(filepath.Join(os.Getenv("HOME"), ".config/neon-contents-grapple.yaml"))

// LoadConfig loads configuration from json file
func LoadConfig(filename string) *Configuration {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	config := &Configuration{}
	err = yaml.Unmarshal(data, config)
	if err != nil {
		panic(err)
	}
	return config
}

// DBInitString returns a sql.Open string.
func (config *Configuration) DBOpenString() string {
	return fmt.Sprintf("user=%s password=%s dbname=%s sslmode=%s",
		config.DBUser, config.DBPassword, config.DBName, config.SSLMode)
}
